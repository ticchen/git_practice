git_dir=$(shell git rev-parse --show-toplevel)

help:
	@echo -e 'Usage:'
	@echo -e '\t add_upstream: create fake remote named as "upstream"'
	@echo -e '\t del_upstream: remove fake remote named as "upstream"'
	@echo -e '\t show_upstream: show upstream information'


###################################################
# prepare
###################################################
prepare:
	git config alias.lg  "log --graph --color --pretty=format:'%x1b[31m%h%x1b[32m%d%x1b[0m%x20%s'"
	git config alias.lga "log --graph --color --pretty=format:'%x1b[31m%h%x1b[32m%d%x1b[0m%x20%s' --all"


###################################################
# upstream server
###################################################
upstream_path=$(git_dir)/.git/upstream
add_upstream:
	if [ ! -f $(upstream_path)/config ]; then \
		mkdir $(upstream_path); \
		git init $(upstream_path) --bare; \
	fi
	git remote add upstream file://$(upstream_path)

del_upstream:
	git remote rm upstream
	rm -rf $(upstream_path)

show_upstream:
	git remote show upstream

define lesson1_text
############################################################
# lesson1: git push
#
#	Yes, You fisish your GREAT job by patch_bbb.
#	Now, you need to push them to the relative remote branch on upstream. (upstream/lesson1)
#	Just push to upstream in fast-forwarding way :)
#
#	commnad: git push
#
############################################################
endef
export lesson1_text

lesson1_help:
	echo "$$lesson1_text"

lesson1: lesson1_close
	git checkout -f -b lesson1 master
	make source_aaa_patch
	git push -u upstream lesson1

	make source_bbb_patch

	make lesson1_help

lesson1_close:
	-git checkout master
	-git branch -D lesson1
	-git push upstream :lesson1


define lesson2_text
############################################################
# lesson2: git push with auto-merge.
#
#	Yes, You fisish your GREAT job by patch_ccc.
#	Now, you need to push them to the relative remote branch on upstream. (upstream/lesson2)
#	But the remote branch has newer commits. (patch_aaa and patch_bbb)
#	you need to move(rebase) your contribution onto the remote one.
#	And then, your can push to upstream in fast-forwarding way.
#
#  	No conflict would happen.
#	Because git would auto merge :)
#
#	commnad:
#			git pull --rebase
#			git push
#			----OR----
#			git fetch
#			git rebase upstream/lesson2
#			git push
#
############################################################
endef
export lesson2_text

lesson2_help:
	echo "$$lesson2_text"

lesson2: lesson2_close
	git checkout -f -b lesson2 master
	make source_aaa_patch
	make source_bbb_patch
	git push -u upstream lesson2

	git reset --hard HEAD^
	make source_ccc_patch

	make lesson2_help

lesson2_close:
	-git checkout master
	-git branch -D lesson2
	-git push upstream :lesson2


define lesson3_text
############################################################
# lesson3: git push with conflict fixed.
#
#	Yes, You fisish your GREAT job by adding "xxx" in b().
#	Now, you need to push them to the relative remote branch on upstream. (upstream/lesson3)
#	But the remote branch has newer commits. (patch_aaa, patch_bbb and patch_ccc)
#	Obviously, you need to move(rebase) your contribution onto the remote one.
#	And then, your can push to upstream in fast-forwarding way.
#
#	A conflict would happen in function b().
#	Try to fixed it :)
#
#	commnad:
#			git pull --rebase
#			(fix conflict)
#			git push
#			----OR----
#			git fetch
#			git rebase upstream/lesson3
#			(fix conflict)
#			git push
#
############################################################
endef
export lesson3_text

lesson3_help:
	echo "$$lesson3_text"

lesson3: lesson3_close
	git checkout -f -b lesson3 master
	make source_aaa_patch
	make source_bbb_patch
	make source_ccc_patch
	git push -u upstream lesson3

	git reset --hard HEAD~2
	make source_xxx_patch

	make lesson3_help

lesson3_close:
	-git checkout master
	-git branch -D lesson3
	-git push upstream :lesson3


define lesson4_text
############################################################
# lesson4: cherry-pick with auto merge.
#
#	Threre are aaa_patch and bbb_patch on the "lesson4_patch" branch.
#	Try to cherry-pick them into "lesson4" branch.
#
#	In this lesson, you should cherry-pick without troubles.
#
############################################################
endef
export lesson4_text

lesson4_help:
	echo "$$lesson4_text"

lesson4: lesson4_close
	git checkout -f -b lesson4 master
	make source_aaa_patch
	make source_bbb_patch
	git branch lesson4_patch

	git reset --hard HEAD~2
	make source_ccc_patch

	make lesson4_help

lesson4_close:
	-git checkout master
	-git branch -D lesson4
	-git branch -D lesson4_patch

define lesson5_text
############################################################
# lesson5: cherry-pick with conflict fixed.
#
#	Threre are aaa_patch and bbb_patch on the "lesson5_patch" branch.
#	Try to cherry-pick them into "lesson5" branch.
#
#	In this lesson, you would need to fix merge conflict when cherry-pick.
#
############################################################
endef
export lesson5_text

lesson5_help:
	echo "$$lesson5_text"

lesson5: lesson5_close
	git checkout -f -b lesson5 master
	make source_aaa_patch
	make source_bbb_patch
	git branch lesson5_patch

	git reset --hard HEAD~2
	make source_xxx_patch

	make lesson5_help

lesson5_close:
	-git checkout master
	-git branch -D lesson5
	-git branch -D lesson5_patch


define lesson6_text
############################################################
# lesson6: track file status
#
#    1. command: git diff
#    <HEAD>===============<INDEX>===============<WORKING>
#         git diff --cached          git diff
#		  ----------------       ---------------
#                    git diff HEAD
#         --------------------------------------
#
#    2. command: git reset
#
#    3. command: git diff 
#    <HEAD>=====================================<WORKING>
#                    git diff
#                    git diff HEAD
#         --------------------------------------
#    4. command: git add -p
#
############################################################
endef
export lesson6_text

lesson6_help:
	echo "$$lesson6_text"

_lesson6: lesson6_close
	git checkout -f -b lesson6 master
	make modify_aaa
	git add source.c
	make modify_bbb

lesson6: _lesson6
	make lesson6_help	

lesson6_close:
	-git reset
	-git checkout master
	-git branch -D lesson6

define lesson6_1_text
############################################################
# lesson6_1: rollback file's content
#
#    1. command: git checkout
#    <BASE>===============<INDEX>===============<WORKING>
#                    git checkout 
#         <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
############################################################
endef
export lesson6_1_text

lesson6_1_help:
	echo "$$lesson6_1_text"

lesson6_1: _lesson6
	make lesson6_1_help


define lesson6_2_text
############################################################
# lesson6_2: reset index status
#
#    1. command: git reset
#    <BASE>===============<INDEX>===============<WORKING>
#            git reset
#          <<<<<<<<<<<<<<<
#
############################################################
endef
export lesson6_2_text

lesson6_2_help:
	echo "$$lesson6_2_text"

lesson6_2: _lesson6
	make lesson6_2_help


define lesson6_3_text
############################################################
# lesson6_3: reset everything :(
#
#    1. command: git reset --hard  # Dangerous
#    <BASE>===============<INDEX>===============<WORKING>
#            git reset --hard   
#          <<<<<<<<<<<<<<<
#          <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
#
############################################################
endef
export lesson6_3_text

lesson6_3_help:
	echo "$$lesson6_3_text"

lesson6_3: _lesson6
	make lesson6_3_help

define lesson_branch_text
############################################################
# lesson_branch:
#
#    0. show branch
#         git branch     # show local branch
#         git branch -r  # show remove branch
#         git branch -a  # show all branch
#    1. create local branch
#         git branch <new_branch> <base_branch>
#         git branch <new_branch>
#    2. switch local branch
#         git checkout <branch>
#    3. create and switch local branch
#         git checkout -b <new_branch>
#         git checkout -b <new_branch> <base_branch>
#    4. delete local branch
#         git checkout -d <branch>
#         git checkout -D <branch>	# Hard way
#
#    5. create remote branch
#         git push <remote> <local_branch>:<remote_branch>
#         git push <remote> <local_branch>
#    6. delete remote branch
#         git push <remote> :<remote_branch>
#    7. set a local branch to track a remote branch
#         git branch --set-upstream <local_branch> <remote_branch>
#         git push -u <remote> <local_branch>:<remote_branch>
#    8. show track information
#         git config -l | grep ^branch
#         git remote show <remote>
#
############################################################
endef
export lesson_branch_text

lesson_branch_help:
	echo "$$lesson_branch_text"


define lesson_useful_text
############################################################
# lesson_useful:
#
#    git diff -<n>
#    git diff -p
#    git status -s
#    git status --ignored
#    git ls-files
#    git grep -A <n> -B <n> -C <n> <pattern>  # line range
#    git grep -i <pattern>	# Ignore case
#    git grep -p <pattern>  # --show-function
#    git clean -xf        # or git clean -Xf
#    git clean -xfd       # or git clean -Xfd
#    git rev-parse --show-toplevel   # GIT_SRC_DIR
#    git merge-base <branch_A> <branch_B>     # common ancestor 
#
############################################################
endef
export lesson_useful_text

lesson_useful_help:
	echo "$$lesson_useful_text"
	
lesson_useful: lesson_useful_help


############################################################
# source code patch
############################################################
modify_aaa:
	sed -i -e "s/aaa/\0\0/" source.c
source_aaa_patch: modify_aaa
	git commit source.c -m "source: add more aaa in a()"

modify_bbb:
	sed -i -e "s/bbb/\0\0/" source.c
source_bbb_patch: modify_bbb
	git commit source.c -m "source: add more bbb in b()"

modify_ccc:
	sed -i -e "s/ccc/\0\0/" source.c
source_ccc_patch: modify_ccc
	git commit source.c -m "source: add more ccc in c()"

modify_xxx:
	sed -i -e '/bbb/a \\tprintf("xxxx\\n");' source.c
source_xxx_patch: modify_xxx
	git commit source.c -m "source: add xxx in b()"


